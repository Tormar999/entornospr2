package Lib;

import java.util.Scanner;

public class Lib1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		char caracter = pedirCaracter(input);
		System.out.println("El caracter leido es: " + caracter);
		input.close();
	}

	private static char pedirCaracter(Scanner input) {
		String cadena;

		do{
			System.out.println("Introduce un solo caracter");
			cadena =  input.nextLine();
		}while(cadena.length() != 1);
		
		return cadena.charAt(0);
	}

}



