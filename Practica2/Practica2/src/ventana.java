import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import javax.swing.JToolBar;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JComboBox;
import javax.swing.JMenu;
import java.awt.TextArea;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import java.awt.Button;
import java.awt.Panel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField txtSelotrop;
	private JTextField txtYopuka;
	private JTextField txtVacio;
	private JTextField txtVacio_1;
	private JTextField txtVacio_2;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	private JTextField textField_19;
	private JTextField textField_20;
	private JTextField textField_21;
	private JTextField textField_22;
	private JTextField textField_23;
	private JTextField textField_24;
	private JTextField textField_25;
	private JTextField textField_26;
	private JTextField textField_27;
	private JTextField textField_28;
	private JTextField textField_29;
	private JTextField textField_30;
	private JTextField textField_31;
	private JTextField textField_32;
	private JTextField textField_33;
	private JTextField textField_34;
	private JTextField textField_35;
	private JTextField textField_36;
	private JTextField textField_37;
	private JTextField textField_38;
	private JTextField textField_39;
	private JTextField textField_40;
	private JTextField textField_41;
	private JTextField textField_42;
	private JTextField textField_43;
	private JTextField textField_44;
	private JTextField textField_45;
	private JTextField textField_46;
	private JTextField textField_47;
	private JTextField textField_48;
	private JTextField textField_49;
	private JTextField textField_50;
	private JTextField textField_51;
	private JTextField textField_52;
	private JTextField textField_53;
	private JTextField textField_54;
	private JTextField textField_55;
	private JTextField textField_56;
	private JTextField textField_57;
	private JTextField textField_58;
	private JTextField textField_59;
	private JTextField textField_60;
	private JTextField textField_61;
	private JTextField textField_62;
	private JTextField textField_63;
	private JTextField textField_64;
	private JTextField textField_65;
	private JTextField textField_66;
	private JTextField textField_67;
	private JTextField textField_68;
	private JTextField textField_69;
	private JTextField textField_70;
	private JTextField textField_71;
	private JTextField textField_72;
	private JTextField textField_73;
	private JTextField textField_74;
	private JTextField textField_75;
	private JTextField textField_76;
	private JTextField textField_77;
	private JTextField textField_78;
	private JTextField textField_79;
	private JTextField textField_80;
	private JTextField textField_81;
	private JTextField textField_82;
	private JTextField textField_83;
	private JTextField textField_84;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana frame = new ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ventana() {
		addContainerListener(new ContainerAdapter() {
			@Override
			public void componentAdded(ContainerEvent arg0) {
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 984, 548);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnOpciones = new JMenu("Opciones");
		menuBar.add(mnOpciones);
		
		JMenuItem mntmSoporte = new JMenuItem("Soporte");
		mnOpciones.add(mntmSoporte);
		
		JMenuItem mntmCerrarSesin = new JMenuItem("Cerrar sesi\u00F3n");
		mnOpciones.add(mntmCerrarSesin);
		
		JMenuItem mntmAtajos = new JMenuItem("Atajos");
		mnOpciones.add(mntmAtajos);
		
		JMenuItem mntmGraficos = new JMenuItem("Graficos");
		mnOpciones.add(mntmGraficos);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 966, 59);
		contentPane.add(toolBar);
		
		JButton btnCopiar = new JButton("Borrar");
		toolBar.add(btnCopiar);
		
		JButton btnCortar = new JButton("Tranladar");
		toolBar.add(btnCortar);
		
		JButton btnPegar = new JButton("Editar");
		toolBar.add(btnPegar);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(542, 213, 412, 278);
		contentPane.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Personaje", null, panel_1, null);
		panel_1.setLayout(null);
		
		JTextPane txtpnClases = new JTextPane();
		txtpnClases.setEditable(false);
		txtpnClases.setText("         Clases");
		txtpnClases.setBounds(27, 151, 118, 29);
		panel_1.add(txtpnClases);
		
		textField = new JTextField();
		textField.setBounds(29, 55, 116, 29);
		panel_1.add(textField);
		
		JTextPane txtpnNombre = new JTextPane();
		txtpnNombre.setText("         Nombre");
		txtpnNombre.setEditable(false);
		txtpnNombre.setBounds(27, 13, 118, 29);
		panel_1.add(txtpnNombre);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(248, 158, 30, 22);
		panel_1.add(spinner);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setBounds(248, 200, 30, 22);
		panel_1.add(spinner_1);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setBounds(377, 200, 30, 22);
		panel_1.add(spinner_2);
		
		JSpinner spinner_3 = new JSpinner();
		spinner_3.setBounds(377, 158, 30, 22);
		panel_1.add(spinner_3);
		
		JTextPane txtpnFuerza = new JTextPane();
		txtpnFuerza.setText("Fuerza");
		txtpnFuerza.setEditable(false);
		txtpnFuerza.setBounds(179, 158, 64, 22);
		panel_1.add(txtpnFuerza);
		
		JTextPane txtpnMagia = new JTextPane();
		txtpnMagia.setText("Magia");
		txtpnMagia.setEditable(false);
		txtpnMagia.setBounds(179, 200, 64, 22);
		panel_1.add(txtpnMagia);
		
		JTextPane txtpnDestreza = new JTextPane();
		txtpnDestreza.setText("Destreza");
		txtpnDestreza.setEditable(false);
		txtpnDestreza.setBounds(301, 158, 64, 22);
		panel_1.add(txtpnDestreza);
		
		JTextPane txtpnSuerte = new JTextPane();
		txtpnSuerte.setText("Suerte");
		txtpnSuerte.setEditable(false);
		txtpnSuerte.setBounds(301, 200, 64, 22);
		panel_1.add(txtpnSuerte);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setToolTipText("");
		comboBox.setEditable(true);
		comboBox.setBounds(27, 193, 116, 29);
		panel_1.add(comboBox);
		
		JButton btnCrear = new JButton("Crear");
		btnCrear.setBounds(224, 57, 97, 25);
		panel_1.add(btnCrear);
		
		TextArea textArea = new TextArea();
		tabbedPane.addTab("Biografia Del Personaje", null, textArea, null);
		
		Button button = new Button("Jugar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setBounds(410, 467, 79, 24);
		contentPane.add(button);
		
		JTabbedPane tabbedPane_1 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_1.setBounds(542, 73, 412, 124);
		contentPane.add(tabbedPane_1);
		
		JPanel panel = new JPanel();
		tabbedPane_1.addTab("Lista de personajes", null, panel, null);
		panel.setLayout(null);
		
		txtSelotrop = new JTextField();
		txtSelotrop.setText("Selotrop");
		txtSelotrop.setBounds(12, 34, 60, 29);
		panel.add(txtSelotrop);
		
		txtYopuka = new JTextField();
		txtYopuka.setText("Yopuka");
		txtYopuka.setBounds(81, 34, 60, 29);
		panel.add(txtYopuka);
		
		txtVacio = new JTextField();
		txtVacio.setText("Vacio");
		txtVacio.setBounds(153, 34, 60, 29);
		panel.add(txtVacio);
		
		txtVacio_1 = new JTextField();
		txtVacio_1.setText("Vacio");
		txtVacio_1.setBounds(224, 34, 60, 29);
		panel.add(txtVacio_1);
		
		txtVacio_2 = new JTextField();
		txtVacio_2.setText("Vacio");
		txtVacio_2.setBounds(296, 34, 60, 29);
		panel.add(txtVacio_2);
		
		JTabbedPane tabbedPane_2 = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane_2.setBounds(10, 72, 520, 371);
		contentPane.add(tabbedPane_2);
		
		JPanel panel_2 = new JPanel();
		tabbedPane_2.addTab("Lista de amigos", null, panel_2, null);
		panel_2.setLayout(null);
		
		textField_1 = new JTextField();
		textField_1.setBounds(12, 13, 116, 29);
		panel_2.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(12, 55, 116, 29);
		panel_2.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setBounds(12, 97, 116, 29);
		panel_2.add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setBounds(12, 138, 116, 29);
		panel_2.add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setBounds(12, 183, 116, 29);
		panel_2.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setBounds(12, 225, 116, 29);
		panel_2.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setBounds(12, 271, 116, 29);
		panel_2.add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setBounds(141, 13, 116, 29);
		panel_2.add(textField_8);
		
		textField_9 = new JTextField();
		textField_9.setBounds(141, 55, 116, 29);
		panel_2.add(textField_9);
		
		textField_10 = new JTextField();
		textField_10.setBounds(141, 97, 116, 29);
		panel_2.add(textField_10);
		
		textField_11 = new JTextField();
		textField_11.setBounds(141, 138, 116, 29);
		panel_2.add(textField_11);
		
		textField_12 = new JTextField();
		textField_12.setBounds(141, 183, 116, 29);
		panel_2.add(textField_12);
		
		textField_13 = new JTextField();
		textField_13.setBounds(141, 225, 116, 29);
		panel_2.add(textField_13);
		
		textField_14 = new JTextField();
		textField_14.setBounds(141, 271, 116, 29);
		panel_2.add(textField_14);
		
		textField_15 = new JTextField();
		textField_15.setBounds(269, 13, 116, 29);
		panel_2.add(textField_15);
		
		textField_16 = new JTextField();
		textField_16.setBounds(269, 55, 116, 29);
		panel_2.add(textField_16);
		
		textField_17 = new JTextField();
		textField_17.setBounds(269, 97, 116, 29);
		panel_2.add(textField_17);
		
		textField_18 = new JTextField();
		textField_18.setBounds(269, 138, 116, 29);
		panel_2.add(textField_18);
		
		textField_19 = new JTextField();
		textField_19.setBounds(269, 183, 116, 29);
		panel_2.add(textField_19);
		
		textField_20 = new JTextField();
		textField_20.setBounds(269, 225, 116, 29);
		panel_2.add(textField_20);
		
		textField_21 = new JTextField();
		textField_21.setBounds(269, 271, 116, 29);
		panel_2.add(textField_21);
		
		textField_22 = new JTextField();
		textField_22.setBounds(397, 13, 116, 29);
		panel_2.add(textField_22);
		
		textField_23 = new JTextField();
		textField_23.setBounds(397, 55, 116, 29);
		panel_2.add(textField_23);
		
		textField_24 = new JTextField();
		textField_24.setBounds(397, 97, 116, 29);
		panel_2.add(textField_24);
		
		textField_25 = new JTextField();
		textField_25.setBounds(397, 138, 116, 29);
		panel_2.add(textField_25);
		
		textField_26 = new JTextField();
		textField_26.setBounds(397, 183, 116, 29);
		panel_2.add(textField_26);
		
		textField_27 = new JTextField();
		textField_27.setBounds(397, 225, 116, 29);
		panel_2.add(textField_27);
		
		textField_28 = new JTextField();
		textField_28.setBounds(397, 271, 116, 29);
		panel_2.add(textField_28);
		
		JPanel panel_3 = new JPanel();
		tabbedPane_2.addTab("Bloqueados", null, panel_3, null);
		panel_3.setLayout(null);
		
		textField_29 = new JTextField();
		textField_29.setBounds(12, 13, 116, 29);
		panel_3.add(textField_29);
		
		textField_30 = new JTextField();
		textField_30.setBounds(12, 55, 116, 29);
		panel_3.add(textField_30);
		
		textField_31 = new JTextField();
		textField_31.setBounds(12, 97, 116, 29);
		panel_3.add(textField_31);
		
		textField_32 = new JTextField();
		textField_32.setBounds(12, 138, 116, 29);
		panel_3.add(textField_32);
		
		textField_33 = new JTextField();
		textField_33.setBounds(12, 183, 116, 29);
		panel_3.add(textField_33);
		
		textField_34 = new JTextField();
		textField_34.setBounds(12, 225, 116, 29);
		panel_3.add(textField_34);
		
		textField_35 = new JTextField();
		textField_35.setBounds(12, 271, 116, 29);
		panel_3.add(textField_35);
		
		textField_36 = new JTextField();
		textField_36.setBounds(146, 13, 116, 29);
		panel_3.add(textField_36);
		
		textField_37 = new JTextField();
		textField_37.setBounds(146, 55, 116, 29);
		panel_3.add(textField_37);
		
		textField_38 = new JTextField();
		textField_38.setBounds(146, 97, 116, 29);
		panel_3.add(textField_38);
		
		textField_39 = new JTextField();
		textField_39.setBounds(146, 138, 116, 29);
		panel_3.add(textField_39);
		
		textField_40 = new JTextField();
		textField_40.setBounds(146, 183, 116, 29);
		panel_3.add(textField_40);
		
		textField_41 = new JTextField();
		textField_41.setBounds(146, 225, 116, 29);
		panel_3.add(textField_41);
		
		textField_42 = new JTextField();
		textField_42.setBounds(146, 271, 116, 29);
		panel_3.add(textField_42);
		
		textField_43 = new JTextField();
		textField_43.setBounds(274, 13, 116, 29);
		panel_3.add(textField_43);
		
		textField_44 = new JTextField();
		textField_44.setBounds(274, 55, 116, 29);
		panel_3.add(textField_44);
		
		textField_45 = new JTextField();
		textField_45.setBounds(274, 97, 116, 29);
		panel_3.add(textField_45);
		
		textField_46 = new JTextField();
		textField_46.setBounds(274, 138, 116, 29);
		panel_3.add(textField_46);
		
		textField_47 = new JTextField();
		textField_47.setBounds(274, 183, 116, 29);
		panel_3.add(textField_47);
		
		textField_48 = new JTextField();
		textField_48.setBounds(274, 225, 116, 29);
		panel_3.add(textField_48);
		
		textField_49 = new JTextField();
		textField_49.setBounds(274, 271, 116, 29);
		panel_3.add(textField_49);
		
		textField_50 = new JTextField();
		textField_50.setBounds(402, 13, 116, 29);
		panel_3.add(textField_50);
		
		textField_51 = new JTextField();
		textField_51.setBounds(402, 55, 116, 29);
		panel_3.add(textField_51);
		
		textField_52 = new JTextField();
		textField_52.setBounds(402, 97, 116, 29);
		panel_3.add(textField_52);
		
		textField_53 = new JTextField();
		textField_53.setBounds(402, 138, 116, 29);
		panel_3.add(textField_53);
		
		textField_54 = new JTextField();
		textField_54.setBounds(402, 183, 116, 29);
		panel_3.add(textField_54);
		
		textField_55 = new JTextField();
		textField_55.setBounds(402, 225, 116, 29);
		panel_3.add(textField_55);
		
		textField_56 = new JTextField();
		textField_56.setBounds(402, 271, 116, 29);
		panel_3.add(textField_56);
		
		JPanel panel_4 = new JPanel();
		tabbedPane_2.addTab("Ignorados", null, panel_4, null);
		panel_4.setLayout(null);
		
		textField_57 = new JTextField();
		textField_57.setBounds(12, 24, 116, 29);
		panel_4.add(textField_57);
		
		textField_58 = new JTextField();
		textField_58.setBounds(12, 66, 116, 29);
		panel_4.add(textField_58);
		
		textField_59 = new JTextField();
		textField_59.setBounds(12, 108, 116, 29);
		panel_4.add(textField_59);
		
		textField_60 = new JTextField();
		textField_60.setBounds(12, 149, 116, 29);
		panel_4.add(textField_60);
		
		textField_61 = new JTextField();
		textField_61.setBounds(12, 194, 116, 29);
		panel_4.add(textField_61);
		
		textField_62 = new JTextField();
		textField_62.setBounds(12, 236, 116, 29);
		panel_4.add(textField_62);
		
		textField_63 = new JTextField();
		textField_63.setBounds(12, 282, 116, 29);
		panel_4.add(textField_63);
		
		textField_64 = new JTextField();
		textField_64.setBounds(142, 24, 116, 29);
		panel_4.add(textField_64);
		
		textField_65 = new JTextField();
		textField_65.setBounds(142, 66, 116, 29);
		panel_4.add(textField_65);
		
		textField_66 = new JTextField();
		textField_66.setBounds(142, 108, 116, 29);
		panel_4.add(textField_66);
		
		textField_67 = new JTextField();
		textField_67.setBounds(142, 149, 116, 29);
		panel_4.add(textField_67);
		
		textField_68 = new JTextField();
		textField_68.setBounds(142, 194, 116, 29);
		panel_4.add(textField_68);
		
		textField_69 = new JTextField();
		textField_69.setBounds(142, 236, 116, 29);
		panel_4.add(textField_69);
		
		textField_70 = new JTextField();
		textField_70.setBounds(142, 282, 116, 29);
		panel_4.add(textField_70);
		
		textField_71 = new JTextField();
		textField_71.setBounds(269, 24, 116, 29);
		panel_4.add(textField_71);
		
		textField_72 = new JTextField();
		textField_72.setBounds(269, 66, 116, 29);
		panel_4.add(textField_72);
		
		textField_73 = new JTextField();
		textField_73.setBounds(269, 108, 116, 29);
		panel_4.add(textField_73);
		
		textField_74 = new JTextField();
		textField_74.setBounds(269, 149, 116, 29);
		panel_4.add(textField_74);
		
		textField_75 = new JTextField();
		textField_75.setBounds(269, 194, 116, 29);
		panel_4.add(textField_75);
		
		textField_76 = new JTextField();
		textField_76.setBounds(269, 236, 116, 29);
		panel_4.add(textField_76);
		
		textField_77 = new JTextField();
		textField_77.setBounds(269, 282, 116, 29);
		panel_4.add(textField_77);
		
		textField_78 = new JTextField();
		textField_78.setBounds(399, 24, 116, 29);
		panel_4.add(textField_78);
		
		textField_79 = new JTextField();
		textField_79.setBounds(399, 66, 116, 29);
		panel_4.add(textField_79);
		
		textField_80 = new JTextField();
		textField_80.setBounds(399, 108, 116, 29);
		panel_4.add(textField_80);
		
		textField_81 = new JTextField();
		textField_81.setBounds(399, 149, 116, 29);
		panel_4.add(textField_81);
		
		textField_82 = new JTextField();
		textField_82.setBounds(399, 194, 116, 29);
		panel_4.add(textField_82);
		
		textField_83 = new JTextField();
		textField_83.setBounds(399, 236, 116, 29);
		panel_4.add(textField_83);
		
		textField_84 = new JTextField();
		textField_84.setBounds(399, 282, 116, 29);
		panel_4.add(textField_84);
	}
	
	
	
	
	
}
